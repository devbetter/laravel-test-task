<?php

namespace Tests\Unit;

use App\Company;
use App\Country;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserCreating()
    {
        $user = factory(User::class)->create();
        $user->save();
        $createdUser = User::find($user->id);
        $this->assertEquals($user->getAttributes(), $createdUser->getAttributes());
    }

    public function testCreateUserWithCompanies()
    {
        $user = factory(User::class)->create();
        $user->save();

        $country = factory(Country::class)->create();
        $country->save();

        $companiesCount = random_int(3, 10);

        factory(Company::class, $companiesCount)->make([
            'country_id' => $country->id
        ])->each(function ($company) use ($user){
            $user->companies()->save($company);
        });

        $this->assertEquals($companiesCount, $user->companies()->count());
    }
}
