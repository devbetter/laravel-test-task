<?php

namespace Tests\Unit;

use App\Company;
use App\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CountryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateCountry()
    {
        $country = factory(Country::class)->create();
        $country->save();
        $createdCountry = Country::find($country->id);
        $this->assertEquals($country->getAttributes(), $createdCountry->getAttributes());
    }

    public function testCreateCountryWithCompanies()
    {
        $country = factory(Country::class)->create();
        $country->save();

        $companiesCount = random_int(3, 10);

        factory(Company::class, $companiesCount)->make()->each(function ($company) use ($country){
            $country->companies()->save($company);
        });

        $this->assertEquals($companiesCount, $country->companies()->count());
    }

    public function testRemoveCountryAndAttachedCompanies()
    {
        $country = factory(Country::class)->create();
        $country->save();

        $country->delete();
        $removedCountry = Country::where('id', $country->id)->exists();
        $this->assertFalse($removedCountry);

        //Check if companies was removed
        $companiesExists = Company::where('country_id', $country->id)->exists();
        $this->assertFalse($companiesExists);
    }
}
