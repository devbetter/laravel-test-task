<?php

namespace Tests\Feature;

use App\Company;
use App\Country;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     * @throws \Exception
     */
    public function testGetUsersByCountryName()
    {
        $country = factory(Country::class)->create();
        $country->save();

        $companies = factory(Company::class, 10)->make([
            'country_id' => $country->id
        ])->each(function ($company) use ($country){
            $country->companies()->save($company);
        });

        $responseData = [
            'users' => []
        ];
        factory(User::class, 1)->make()->each(function ($user) use ($companies, &$responseData){
            $user->save();
            $responseItem = [
                'id'        => $user->id,
                'companies' => []
            ];
            $companies->random(random_int(1, 5))->each(function ($company) use ($user, &$responseItem){
                $associatedAt = Carbon::now()->subMinutes(random_int(60, 60 * 24 * 30));
                $user->companies()->attach($company, ['associated_at' => $associatedAt]);
                $responseItem['companies'][] = [
                    'id'            => $company->id,
                    'name'          => $company->name,
                    'associated_at' => $associatedAt->format('Y-m-d H:i:s')
                ];
            });
            $responseData['users'][] = $responseItem;
        });
        $response = $this->getJson('/users?country=' . $country->name);

        $response->assertJson($responseData);
    }
}
