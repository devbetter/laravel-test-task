<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class FileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpload()
    {
        Storage::fake('local');

        $response = $this->json('POST', '/upload', [
            'file' => UploadedFile::fake()->create('test.pdf')
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['isUploaded', 'path']);
        $path = $response->json('path');
        if ($response->json('isUploaded')) {
            Storage::disk('local')->assertExists($path);
        } else {
            $this->assertNull($path);
        };
    }

    public function testUploadFailed()
    {
        Storage::fake('local');

        $response = $this->json('POST', '/upload');
        $response->assertStatus(422);

        $response = $this->json('POST', '/upload', [
            'file' => UploadedFile::fake()->create('invalid-file.txt')
        ]);
        $response->assertStatus(422);
    }
}
