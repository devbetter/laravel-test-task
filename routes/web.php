<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\File;
use App\Http\Requests\UploadPDF;
use App\User;
use Illuminate\Http\Request;

Route::get('/', function (){
    return view('welcome');
});

Route::get('/users', function (Request $request){
    $users = User::findByCountryName($request->get('country'))
        ->with('companies')
        ->get();

    return response()->json(['users' => User::preprocessSearchResults($users)]);
});


Route::post('/upload', function (UploadPDF $request){
    $file = $request->file('file');

    return response()->json(File::upload($file));
});
