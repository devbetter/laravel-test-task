<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function companies()
    {

        return $this->belongsToMany(Company::class, 'user_company')->withPivot('associated_at');
    }

    /**
     * Find users by coutry name (through by Company)
     * @param $query
     * @param $name
     */
    public function scopeFindByCountryName($query, $name)
    {
        $query->whereHas('companies', function ($companyQuery) use ($name){
            $companyQuery->whereHas('country', function ($countryQuery) use ($name){
                $countryQuery->where('name', $name);
            });
        });
    }

    public static function preprocessSearchResults(Collection $data): Collection
    {
        return $data->map(function ($user){
            return [
                'id'        => $user->id,
                'companies' => $user->companies->map(function ($company){
                    return [
                        'id'            => $company->id,
                        'name'          => $company->name,
                        'associated_at' => $company->pivot->associated_at
                    ];
                })
            ];
        });
    }
}
