<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    protected $fillable = ['name', 'path', 'size'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        //remove old file if path was updated
        static::updated(function ($model){
            /** @var self $model */
            if (!$model->wasChanged(['path'])) {
                return true;
            }

            $oldPath = $model->getOriginal('path');
            if (empty($oldPath)) {
                return true;
            }

            Storage::delete($oldPath);
        });
    }

    public static function searchFor(UploadedFile $file, string $search): bool
    {

        return random_int(0, 1);
    }

    public static function upload(UploadedFile $file): array
    {
        $searchInFile = 'Proposal';

        //ignore file if dont search needed string
        if (!self::searchFor($file, $searchInFile)) {
            return ['isUploaded' => false, 'path' => null];
        }

        // file will renamed to random name
        $path = $file->store('/files');
        $file = File::updateOrCreate([
            'name' => $file->getClientOriginalName(),
            'size' => $file->getSize()
        ], [
            'path' => $path
        ]);

        return ['isUploaded' => true, 'path' => $path, 'isNew' => $file->wasRecentlyCreated];
    }
}
