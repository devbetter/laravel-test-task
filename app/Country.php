<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name'];

    /**
     * Companies relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companies()
    {

        return $this->hasMany(Company::class);
    }
}
