<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name', 'country_id'];

    /**
     * Country relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {

        return $this->belongsTo(Country::class);
    }
}
